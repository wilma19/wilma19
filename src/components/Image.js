import React, { useState } from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import { Modal } from 'react-bootstrap'
import { VscZoomIn } from "react-icons/vsc";


const Image = ({ image }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div style={{ position: 'relative' }}>
        <VscZoomIn size={22} style={{ position: 'absolute', left: 5, top: 5, zIndex: 30, cursor: 'pointer' }} />
        <GatsbyImage
          image={image.localFile.childImageSharp.gatsbyImageData}
          alt={`Image`}
          style={{ width: "100%", marginBottom: "0.5rem" }}
          objectPosition="0% 0%"
          onClick={handleShow}
        />
          {image.caption !== image.alternativeText && <span className="image-caption">{image.caption}</span>}
      </div>

      <Modal
        centered
        size="lg"
        show={show}
        onHide={handleClose}
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
          <GatsbyImage
            image={image.localFile.childImageSharp.gatsbyImageData}
            alt={`Image`}
          />
          {image.caption !== image.alternativeText && <span className="image-caption">{image.caption}</span>}
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Image
