import React from "react"
import { Row, Col } from "react-bootstrap"
import { useStaticQuery, graphql, Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import BlogRoll from "../components/BlogRoll"
import Content from "../components/Content"


const IndexPage = () => {
  const data = useStaticQuery(graphql `
    query WelcomeQuery {
      allStrapiWilma19Willkommen {
        edges {
          node {
            content { data { content } }
          }
        }
      }
    }
  `)

  const Links = () => (
    <>
      <h4>Links</h4>
      <a className="mb-2" href="https://plattenkosmos.berlin">▸ Aktiv in Lichtenberg e.V.</a><br/>
      <a className="mb-2" href="https://alt-lichtenberg-fan.de/">▸ FAN-Beirat (Frankfurter Allee Nord)</a><br/>
      <a className="mb-2" href="https://licht-blicke.org/">▸ Fach-/Netzwerkstelle Licht-Blicke</a><br/>
      <a className="mb-2" href="https://syndikat.org">▸ Mietshäusersyndikat</a><br/>
      <a className="mb-2" href="http://syndikat.blogsport.eu/">▸ MHS Berlin-Brandenburg</a><br/>
    </>
  )

  const Welcome = () => (
    <Row className="welcome mb-5">
      <Col md={8}>
        <Content>
          {data.allStrapiWilma19Willkommen.edges[0].node.content.data.content}
        </Content>
        <Link to={"über-uns"}>Erfahre hier mehr über uns...</Link>
      </Col>
      <Col md={4} className="mt-3 mt-md-4 pb-4">
        <StaticImage alt="Bild vom Haus" src="../images/magda_cropped.jpg" /> 
      </Col>
    </Row>
  )

  return(
    <Layout sideNavExtra={<Links />}>
      <Welcome />
      <BlogRoll />
    </Layout>
  )
}

export default IndexPage
