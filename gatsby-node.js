const path = require(`path`)

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions

  const result = await graphql(`
    query {
      allStrapiWilma19Post {
        edges {
          node {
            slug
            title
            content { data { content } }
            date(formatString: "DD.MM.YYYY", locale: "de")
            bigHeaderImage {
              caption
              alternativeText
              localFile {
                childImageSharp {
                  gatsbyImageData(width: 1200, placeholder: TRACED_SVG, webpOptions: {quality: 95})
                }
              }
            }
            featuredImage {
              caption
              alternativeText
              localFile {
                childImageSharp {
                  gatsbyImageData(width: 800, placeholder: TRACED_SVG, webpOptions: {quality: 95})
                }
              }
            }
            images {
              caption
              alternativeText
              localFile {
                childImageSharp {
                  gatsbyImageData(width: 800, placeholder: TRACED_SVG, webpOptions: {quality: 95})
                }
              }
            }
          }
        }
      }
      allStrapiWilma19Page {
        edges {
          node {
            slug
            title
            content { data { content } }
            secondary
            images {
              caption
              localFile {
                childImageSharp {
                  gatsbyImageData(width: 800, placeholder: TRACED_SVG, webpOptions: {quality: 95})
                }
              }
            }
          }
        }
      }
    }`
  )

  if (result.errors) {
    reporter.panicOnBuild(
      `There was an error loading your Strapi articles`,
      result.errors
    )

    return
  }

  result.data.allStrapiWilma19Post.edges.forEach(edge => {
    createPage({
      type: "post",
      path: edge.node.slug,
      component: path.resolve(`src/templates/post.js`),
      context: edge.node
    })
  });
  result.data.allStrapiWilma19Page.edges.forEach(edge => {
    createPage({
      type: "page",
      path: edge.node.slug,
      component: path.resolve(`src/templates/page.js`),
      context: edge.node
    })
  });
}